#!/usr/bin/env python

import arena
import agents
import time
import rospy

class Simulation(object):
    def __init__(self, targets = 10, obstacles = 4, simspeed = 1):

        self.simspeed = simspeed
        self.time     = time.time()
        #  self.display  = graphicsDisplay.SimulationGraphics(simspeed)
        self.arena    = arena.Arena()
                
        self.robots   = [agents.Target(x, y, i+1, self.time) for x, y, i in self.arena.targetPoses(targets)]
        self.robots  += [agents.Obstacle(x,y, -i-1, self.time) for x, y, i in self.arena.obstaclePoses(obstacles)]
        

        
        self.robotsIn = 0
        self.robotsOut = 0
        
        
    def run(self):
        """
        Main control loop for simulation.
        """
        #  self.display.initialize(self.arena, self.robots, self)
        self.numMoves = 0
        
        shown = False
                
        # Start simulation
	r = rospy.Rate(15)  # 15hz
        while self.numMoves < 600 or len(self.robots) == 0:
            delta = time.time() - self.time
            self.time = time.time()
            
            # Track progress
            trace = False
            if int(self.numMoves + delta) == int(self.numMoves + 1):
                trace = True
            
            self.numMoves += delta
            
            for robot in self.robots:
                #Solicit an action
                remaining = filter(lambda x: x != robot, self.robots)

                robot.move(delta, remaining)
                
                right = self.arena.leaveRight(robot)
                wrong = self.arena.leaveWrong(robot)
                
                if right:
                    self.robotsIn += 1
                    #  self.display.deleteRobot(self.robots.index(robot))
                    self.robots.remove(robot)
                elif wrong:
                    self.robotsOut += 1
                    #  self.display.deleteRobot(self.robots.index(robot))
                    self.robots.remove(robot)
                #  else:
                    # Change the display
                    #  self.display.update( robot, self.robots.index(robot), self.robotsIn, self.robotsOut, self.numMoves )
                    #  if trace:
                        #  self.display.drawTrace(robot.position)

                        
                if self.robotsIn > 6 and not shown:
                    #  self.display.showMessage(self.time)
                    shown = True
            r.sleep()
            
        #  self.display.finish()
        print "%d robots IN " % self.robotsIn
        print "%d robots OUT" % self.robotsOut
        

