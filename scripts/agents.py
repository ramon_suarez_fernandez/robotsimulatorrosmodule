#!/usr/bin/env python

import random
import math
import rospy
import roslib
roslib.load_manifest('droneMsgsROS')
from geometry_msgs.msg import Vector3Stamped
from droneMsgsROS.msg import robotPose

pub = rospy.Publisher('chatter', robotPose)
id_r = 0

class Robot(object):
    """
    Robots know its position (x, y, z), along with its travelling direction.
    The left corner is (0,0,0). A robot can be waiting because of failure.
    """
        
    def __init__(self, position, direction, radius, types):
        global id_r
        """
        The initial state of the robot is defined with an specific position and direction. 
        """
        self.position  = position
        self.direction = direction
        self.speed     = (0,0)
        self.radius    = radius
        self.waiting   = False
        self.collided  = False
        self.touched   = False

        self.id = id_r + 1
        self.type = types
        
    ###########################
    ### GETTERS AND SETTERS ###
    ###########################

    def getPosition(self):
        return self.position
    
    def setPosition(self, position):
        self.position = position
    
    def getDirection(self):
        return self.direction
    
    def getRadius(self):
        return self.radius
    
    def setRadius(self, radius):
        self.radius = radius
    
    def setDirection(self, direction):
        self.direction = direction
    
    def isWaiting(self):
        return self.waiting
    
    def setWaiting(self, waiting):
        self.waiting = waiting 
        
    def isCollided(self):
        return self.collided
            
    def isTouched(self):
        return self.touched
        
    def setTouched(self, touched):
        print "Is touched"
        self.touched = touched
        
    ###########################
    ##### ACTION METHODS ######
    ###########################
    def move(self, time, robots):
        """
        A robot can move or not depending of its internal state
        The behaviour of the robot is defined inside of its class
        """
        pass
    
    def collide(self, robots):
        """
        Two robots can collide if both robot positions are enough near
        """
        self.collided = False
        x1, y1, z1 = self.position
        for robot in robots:
            x2, y2, z2 = robot.position
            collided = math.pow(x2-x1,2) + math.pow(y1-y2,2) <= math.pow(self.radius+robot.radius,2)
            if collided and z1 == z2:
                # State values
                self.collided = True

        return self.collided
    
    def intersects(self, pos):
        x = pos[0]
        y = pos[1]
        center_x, center_y, center_z = self.position
        square_dist = (center_x - x) ** 2 + (center_y - y) ** 2
        return square_dist <= self.radius ** 2
#         return True
     
    ###########################
    ##### AUXILIAR METHODS ####
    ###########################   
    def __eq__(self, robot):
        """
        Two robots are the same robot if both have the same position and direction
        """
        return self.position == robot.position and self.direction == robot.direction



        
    def collide(self, robots):
#         x1, y1, z1 = self.position
#         for robot in robots:
#             x2, y2, z2 = robot.position
#             collided = math.pow(x2-x1,2) + math.pow(y1-y2,2) <= math.pow(self.radius+robot.radius,2)
#             if collided:
#                 self.collided = True
#                 robot.collided = True
#             if isinstance(robot, Obstacle):
#                 self.waiting = True
#             
#         return self.collided
        return False
    
    def intersects(self, pos):
        return False
    
    def move(self, time, robots):
        """
        A robot can move or not depending of its internal state
        The behaviour of the robot is defined inside of its class
        """
        self.current_time += time
        if self.state:
            res = self.state.perform(time, self.current_time)
            if res:
                if isinstance(self.state, Reaching):
                    self.state = Descending(self.current_time)
                elif isinstance(self.state, Descending):
                    self.state = Magnet(self.robot)
                elif isinstance(self.state, Magnet):
                    self.state = Ascending(self.current_time)
                elif isinstance(self.state, Ascending) :
                    self.state = None
                    self.robot = None
                      

    def touch(self, robot):
        if (self.state is None and self.robot is None) or isinstance(self.state, Reaching):
            self.state = Reaching(self, robot)
            self.robot = robot
            return True
        else:
            return False    
        

class GroundRobot(Robot):
    SPEED = 330
    
    def __init__(self, position, direction, types, initialState):
        Robot.__init__(self, position, direction, 0.17, types)
        self.currentState = None
        self.changeState(initialState)
        self.vr = 0
        self.vl = 0
    
    def changeState(self, newState):
        if (self.currentState != None):
            self.currentState.exit()
        self.currentState = newState
        self.currentState.enter()
        
    def move(self, time, robots):
        self.currentTime += time
        self.collide(robots)
        if (self.currentState != None):
            self.currentState.update()
        self.drive_direct(time)
        
    def drive_direct(self, time):
        x, y, z = self.position
        self.direction += ((float(self.vr-self.vl)/274) * time)
        #self.direction = self.direction % (2*math.pi)
        dx = ((float(self.vr+self.vl)/2)*math.cos(self.direction))/1000*time
        dy = ((float(self.vr+self.vl)/2)*math.sin(self.direction))/1000*time
            
        self.position = (x+dx,y+dy,z)
        self.speed = (dx, dy)
        #print self.id
        pose = robotPose()
        pose.x = -x+10
        pose.y = y-10
        pose.z = 0
        pose.theta = self.direction % (2*math.pi)
        pose.id_Robot = self.type
        if self.type < 0:
            pose.Robot_Type = 0
        else:
            pose.Robot_Type = 1
        # pub_2.publish(self.direction)
        pub.publish(pose)




    
    def isTimeUp(self, counter, length):
        if (self.currentTime - counter > length):
            return True
        return False
  
    def getCurrentState(self):
        return self.currentState  
  
class Obstacle(GroundRobot):
    collisionLength = 3.3
    
    def __init__(self, position, direction, types, current_time):
        '''
        Constructor
        '''
        GroundRobot.__init__(self, position, direction, types, ObstacleWait(self))
        self.beginCollision  = 0
        self.currentTime = current_time
        
    def intersects(self, pos):
        return False
        
     
class Target(GroundRobot):
    noiseInterval   = 5.0
    reverseInterval = 20.0
    noiseLength     = 0.85
    reverseLength   = 2.465
    topTouchLength  = reverseLength/4
    
    def __init__(self, position, direction, types, current_time):
        '''
        Constructor
        '''
        GroundRobot.__init__(self,position, direction, types, TargetWait(self))
        
        self.currentTime   = current_time
        self.lastNoise     = current_time
        self.lastReverse   = current_time
        self.beginNoise    = 0
        self.beginReverse  = 0
        self.beginTopTouch = 0
    
        
class ObstacleWait:
    def __init__(self, robot):
        self.robot = robot
        
    def enter(self):
        self.robot.vr = 0
        self.robot.vl = 0
    def exit(self):
        pass
    def update(self):
        if not self.robot.isWaiting():
            self.robot.changeState(ObstacleRun(self.robot))
            
class ObstacleRun:
    def __init__(self, robot):
        self.robot = robot
        
    def enter(self):
        self.robot.vr = GroundRobot.SPEED + 9
        self.robot.vl = GroundRobot.SPEED - 9
    def exit(self):
        self.robot.vr = 0
        self.robot.vl = 0
        if self.robot.isCollided():
            x, y, z = self.robot.position
            dx, dy  = self.robot.speed
            
            self.robot.position = (x-dx*1.2, y-dy*1.2, z)
    def update(self):
        if self.robot.isWaiting():
            self.robot.changeState(ObstacleWait(self.robot))
        elif self.robot.isCollided():
            self.robot.changeState(ObstacleCollision(self.robot))    
    
class ObstacleCollision:
    def __init__(self, robot):
        self.robot = robot
        
    def enter(self):
        self.robot.vr = 0
        self.robot.vl = 0        
        self.robot.beginCollision = self.robot.currentTime
    def exit(self):
        pass   
       
    def update(self):
        if self.robot.isTimeUp(self.robot.beginCollision, Obstacle.collisionLength) and not self.robot.isCollided():
            self.robot.beginCollision = self.robot.currentTime
            self.robot.changeState(ObstacleRun(self.robot))
        elif self.robot.isWaiting():
            self.robot.changeState(ObstacleWait(self.robot))

class TargetWait:
    def __init__(self, robot):
        self.robot = robot
        
    def enter(self):
        self.robot.vr = 0
        self.robot.vl = 0
        
    def exit(self):
        self.robot.lastNoise = self.robot.currentTime
        self.robot.lastReverse = self.robot.currentTime
        
    def update(self):
        if not self.robot.isWaiting():
            self.robot.changeState(TargetRun(self.robot))

class TargetRun:
    def __init__(self, robot):
        self.robot = robot
        
    def enter(self):
        self.robot.vr = GroundRobot.SPEED
        self.robot.vl = GroundRobot.SPEED
        
    def exit(self):
        if self.robot.isCollided():
            x, y, z = self.robot.position
            dx, dy  = self.robot.speed
            
            self.robot.position = (x-dx*1.2, y-dy*1.2, z)
        
    def update(self):
        if self.robot.isWaiting():
            self.robot.changeState(TargetWait(self.robot))
        elif self.robot.isCollided():
            self.robot.changeState(TargetCollision(self.robot))
        elif self.robot.isTimeUp(self.robot.lastReverse, Target.reverseInterval):
            self.robot.lastReverse = self.robot.currentTime
            self.robot.changeState(Reverse(self.robot))
        elif self.robot.isTimeUp(self.robot.lastNoise, Target.noiseInterval):
            self.robot.lastNoise = self.robot.currentTime
            self.robot.changeState(TrajectoryNoise(self.robot))
        elif self.robot.isTouched():
            self.robot.touched = False
            self.robot.changeState(TopTouch(self.robot))
                    
class TargetCollision:
    def __init__(self, robot):
        self.robot = robot
        
    def enter(self):
        self.robot.vr = 0
        self.robot.vl = 0
        
    def exit(self):
        pass
        
    def update(self):
        self.robot.changeState(Reverse(self.robot))

class TrajectoryNoise:
    RANDOM_MAX = 64
    def __init__(self, robot):
        self.robot = robot
        
    def enter(self):
        rand = random.randint(0,TrajectoryNoise.RANDOM_MAX)
        offset = rand - TrajectoryNoise.RANDOM_MAX/2
            
        self.robot.vr = GroundRobot.SPEED + offset
        self.robot.vl = GroundRobot.SPEED - offset
        
        self.robot.beginNoise = self.robot.currentTime
    def exit(self):
        pass
        
    def update(self):
        if self.robot.isWaiting():
            self.robot.changeState(TargetWait(self.robot))
        elif self.robot.isTouched():
            self.robot.touched = False
            self.robot.changeState(TopTouch(self.robot))
        elif self.robot.isTimeUp(self.robot.beginNoise, Target.noiseLength):
            self.robot.beginNoise = self.robot.currentTime
            self.robot.changeState(TargetRun(self.robot))
        elif self.robot.isCollided():
            self.robot.changeState(TargetCollision(self.robot))
        
    
class Reverse:
    def __init__(self, robot):
        self.robot = robot
        
    def enter(self):
        self.robot.vr = GroundRobot.SPEED/2
        self.robot.vl = -GroundRobot.SPEED/2
        
        self.robot.beginReverse = self.robot.currentTime
    def exit(self):
        pass
        
    def update(self):
        if self.robot.isWaiting():
            self.robot.changeState(TargetWait(self.robot))
        elif self.robot.isTouched():
            self.robot.touched = False
            self.robot.changeState(TopTouch(self.robot))
        elif self.robot.isTimeUp(self.robot.beginReverse, Target.reverseLength):
            self.robot.beginReverse = self.robot.currentTime
            self.robot.changeState(TargetRun(self.robot))


            
class Reaching:
    fspeed = 1
    def __init__(self,drone,robot):
        self.drone    = drone
        self.robot    = robot

            
            
    def perform(self, time, current_time):                 
        x1, y1, z1 = self.drone.position
        x2, y2, z2 = self.robot.position
        self.drone.direction = math.atan2(y2-y1,x2-x1)

        dx = math.cos(self.drone.direction) * self.fspeed * time
        dy = math.sin(self.drone.direction) * self.fspeed * time
                
        self.drone.position = (x1 + dx, y1 + dy, z1)
        self.drone.speed = (dx,dy)
            
        if (x2 - x1)**2 + (y2 - y1)**2 <= 0.05**2:
            return True
        return False
        
